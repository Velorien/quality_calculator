import 'package:flutter/material.dart';

class QualitySelector extends StatelessWidget {
  final void Function(int) qualitySelected;

  QualitySelector({this.qualitySelected});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: 0,
      child: Column(
          children: <Widget>[
            for (int r = 0; r < 4; r++)
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  for (int c = 1; c <= 5; c++)
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5.0),
                        child: RaisedButton(
                          child: Text('${5 * r + c}'),
                          onPressed: () => qualitySelected(5 * r + c),
                          color: Color.lerp(Colors.orange, Colors.deepOrange,
                              (5 * r + c) / 20.0),
                        ),
                      ),
                    )
                ],
              )
          ]),
    );
  }
}
