import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:quality_calculator/qualityList.dart';
import 'package:quality_calculator/qualitySelector.dart';
import 'package:trotter/trotter.dart';
import 'package:async/async.dart';

List<List<int>> _getCombinations(List<int> qualities) {
  final List<List<int>> result = [];
  final eq = ListEquality().equals;
  for (int i = 2; i <= qualities.length; i++) {
    var combinations = Combinations(i, qualities.asMap().entries.toList());
    for (var c in combinations.iterable) {
      final current = List<MapEntry<int, int>>.from(c)
          .map((x) => x.value)
          .toList()
            ..sort((f, s) => f.compareTo(s));
      if (current.reduce((f, s) => f + s) == 40 &&
          !result.any((x) => eq(x, current))) {
        result.add(current);
      }
    }
  }

  return result;
}

void main() => runApp(QualityCalculator());

class QualityCalculator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Quality Calculator',
      theme: ThemeData(primarySwatch: Colors.orange),
      home: MainView(),
    );
  }
}

class MainView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainViewState();
}

class MainViewState extends State<MainView> {
  final List<int> qualities = [];
  CancelableOperation<List<List<int>>> computation =
      CancelableOperation.fromFuture(Future.value([]));

  @override
  Widget build(BuildContext context) {
    const headerStyle =
        const TextStyle(fontWeight: FontWeight.bold, fontSize: 20);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Quality Calcualtor'),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(child: Text('Qualities')),
              Tab(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 5),
                      child: Text('Combinations'),
                    ),
                    FutureBuilder(
                        future: computation.value,
                        builder: (ctx, snapsthot) {
                          if (snapsthot.connectionState != ConnectionState.done)
                            return SizedBox(
                              height: 30,
                              width: 30,
                              child: CircularProgressIndicator(backgroundColor: Colors.red));
                          return Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                color: Colors.deepOrange,
                                borderRadius: BorderRadius.circular(15)),
                            child: Center(
                                child: Text(snapsthot.data.length.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )),
                          );
                        })
                  ],
                ),
              )
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.delete),
          backgroundColor: Colors.red,
          onPressed: _clearQualities,
        ),
        body: TabBarView(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 16, left: 10, bottom: 12),
                  child: Text(
                    'Add item qualities',
                    style: headerStyle,
                  ),
                ),
                QualitySelector(
                  qualitySelected: _addQuality,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 12, left: 10, bottom: 12),
                  child: Text('Selected qualities', style: headerStyle),
                ),
                QualityList(qualities: qualities, qualityTapped: _removeQuality)
              ],
            ),
            FutureBuilder(
                future: computation.value,
                builder: (ctx, snapshot) {
                  if (snapshot.connectionState != ConnectionState.done)
                    return Center(child: CircularProgressIndicator());
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.data != null)
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (ctx, i) => Card(
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Wrap(
                                      children: <Widget>[
                                        for (var item in snapshot.data[i])
                                          Padding(
                                            padding: const EdgeInsets.all(5.0),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  border: Border.all(
                                                      width: 2,
                                                      color:
                                                          Colors.deepOrange)),
                                              child: Center(
                                                  child: Text(item.toString(),
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 16),
                                                      softWrap: false)),
                                              height: 50,
                                              width: 50,
                                            ),
                                          )
                                      ],
                                    ),
                                  ),
                                  FlatButton(
                                      child: Icon(Icons.delete),
                                      onPressed: () =>
                                          _removeQualities(snapshot.data[i]))
                                ],
                              ),
                            ));
                })
          ],
        ),
      ),
    );
  }

  void _addQuality(int quality) {
    setState(() {
      computation.cancel();
      qualities.add(quality);
      computation =
          CancelableOperation.fromFuture(compute(_getCombinations, qualities));
    });
  }

  void _removeQuality(int index) {
    setState(() {
      computation.cancel();
      qualities.removeAt(index);
      computation =
          CancelableOperation.fromFuture(compute(_getCombinations, qualities));
    });
  }

  void _removeQualities(List<int> toRemove) {
    setState(() {
      computation.cancel();
      for (var q in toRemove) qualities.remove(q);
      computation =
          CancelableOperation.fromFuture(compute(_getCombinations, qualities));
    });
  }

  void _clearQualities() {
    setState(() {
      computation.cancel();
      qualities.clear();
      computation = CancelableOperation.fromFuture(Future.value([]));
    });
  }
}
