import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QualityList extends StatelessWidget {
  final void Function(int) qualityTapped;
  final List<int> qualities;

  QualityList({this.qualityTapped, this.qualities});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: 0,
      child: Wrap(
          alignment: WrapAlignment.start,
          runAlignment: WrapAlignment.start,
          children: <Widget>[
            for (int i = 0; i < qualities.length; i++)
              Padding(
                padding: const EdgeInsets.only(left: 5, top: 5),
                child: Container(
                  height: 50,
                  width: 50,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(25)),
                  child: FlatButton(
                      child: Text(
                        '${qualities[i]}',
                        style: TextStyle(color: Colors.white, fontSize: 16),
                        softWrap: false,
                      ),
                      onPressed: () => qualityTapped(i)),
                ),
              )
          ]),
    );
  }
}
